;;;;
;; package extending standard allegroserve html tags with support
;; for ganelon.mvc forms
;
; $Id$
;
;;;;
(defpackage "GANELON.HTML"
  (:use
	 #:COMMON-LISP
	 #:NET.HTML.GENERATOR
	 #:GANELON.UTILS
	 )
  (:export
	 #:mvc-bean
	 #:mvc-bean-value
	 #:mvc-form
	 #:mvc-input
	 #:mvc-select
	 #:mvc-option
	 #:mvc-radio
	 #:mvc-checkbox
	 #:mvc-textarea
	 )
  )

(in-package :GANELON.HTML)


;;form
(defmacro mvc-form (name action params &rest body)
  "Generates and sets form (in variable named mvc-form, be careful!)
  for use by other controls."
  `(let ((mvc-form (ganelon.mvc:get-form ,name 
													  ganelon.mvc:context
													  ganelon.mvc:session)))
	  (make-tag "form"
					,body
					,params	       
					(cons "name" ,name)
					(cons "action" (ganelon.mvc:get-action-url 
										  ganelon.mvc:project ,action))))
  )

;;input with value
(defmacro mvc-input (name params &key (value nil))
  "HTML input of default type text"
  `(make-tag "input"
				 nil
				 ,params
				 (cons "name" ,name)
				 (cons "value" 
						 (control-value ,name ,value)))
  )

(defmacro mvc-radio (name value &optional params)
  `(make-tag "input"
				 nil
				 ,params
				 (cons "name" ,name)
				 (cons "value" ,value)
				 (cons "type" "radio")
				 (if (equal ,value (control-value ,name nil))
					(cons "checked" "1")))
  )

(defmacro mvc-checkbox (name value &optional params)
  `(make-tag "input"
				 nil
				 ,params
				 (cons "name" ,name)
				 (cons "value" ,value)
				 (cons "type" "checkbox")
				 (if (equal ,value (control-value ,name nil))
					(cons "checked" "1")))
  )
(defmacro mvc-select (name value params &rest body)
  `(let ((select-value (control-value ,name ,value)))
	  (make-tag "select"
					,body
					,params
					(cons "name" ,name)
					)
	  )		      
  )

(defmacro mvc-option (value params &rest body)
  `(make-tag "option"
				 ,body
				 ,params
				 (cons "value" ,value)
				 (if (equal ,value select-value)
					(cons "selected" "1")))

  )

(defmacro mvc-textarea (name &optional value params)
  `(make-tag "textarea"
				 '(net.html.generator:html
					 (:princ (control-value ,name ,value)))
				 ,params)
  )		

(defmacro mvc-bean (property-path &optional context-type)
  `(mvc-bean-value 
	  (case ,context-type
		 (:session (ganelon.mvc:session-fields ganelon.mvc:session))
		 (:context ganelon.mvc:context)
		 (T (if (gethash (car ,property-path) 
							  (ganelon.mvc:session-fields ganelon.mvc:session))
				(ganelon.mvc:session-fields ganelon.mvc:session)
				ganelon.mvc:context)))
	  ,property-path
	  (ganelon.mvc::project-package ganelon.mvc:project))
  )

(defun mvc-bean-value (bean property-path package)
  ;;find a form or hashtable in session/request context 
  (and bean (car property-path) 
		 (get-property bean (car property-path) package)
		 (if (cdr property-path)
			(mvc-bean-value (get-property 
									bean (car property-path) package)
								 (cdr property-path)
								 package)
			(get-property bean (car property-path) package)))
  )

(defun tag-params (params)
  (if params
	 (format nil "~A=\"~A\" ~A" (caar params) (cdar params)
				(tag-params (cdr params)))
	 "")
  )

(defmacro make-tag (name body params &rest addparams)
  `(progn
	  (net.html.generator:html 
		 (:princ
			(format nil 
					  "<~A ~A>" ,name
					  (tag-params 
						 (nconc 
							(list ,@addparams)
							,params))
					  )))
	  ,@body
	  (net.html.generator:html
		 (:princ
			(format nil "</~A>" ,name)))
	  )
  )

(defmacro control-value (name &optional value)
  `(if mvc-form
	  (or ,value
			(slot-value mvc-form
							(symb2 (ganelon.mvc:project-package ganelon.mvc:project)
									 ,name)))
	  (or ,value ""))
  )
		    

;
; $Log$
;
