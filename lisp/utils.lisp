;
; $Id$
;

(defpackage "GANELON.UTILS"
  (:export
   #:get-struct-value
   #:symb
   #:symb2
   #:funcallbyname
   #:strconc
   #:split
   #:get-property
   #:count-back-chars
   #:string-replace
   #:update-struct
   #:contents-of-file
   #:contents-of-stream
	#:split-str
	#:read-chars
	#:make-substrs
	#:update-struct-from-xml
	#:nth-to-int
	#:flatten-list-of-lists
	#:strip-tags
	#:map-nth
   )
)
(in-package :GANELON.UTILS)

;;;translate string into symbol
(defmacro symb (&rest args)
  `(values (intern (string-upcase 
		    (concatenate 'string ,@args))))
  )
(defun get-struct-value (form name)
  (slot-value form (symb name))
  )

(defmacro symb2 (package &rest args)
  `(values (intern (string-upcase
		    (concatenate 'string ,@args)) ,package))
  )

(defmacro funcallbyname (name &rest args)
  `(funcall (symb ,name) ,@args)
  )

(defmacro strconc (&rest args)
  `(concatenate 'string ,@args)
  )

(defun split (str delim &optional (limit nil) (start 0))
  (let ((pos (search delim str :start2 start)))
    (if (and pos (or (not limit) (> limit 0)))
	(nconc (list (subseq str start pos))
	       (split str delim (if limit (- limit 1)) (+ pos 1)))
      (list (subseq str start))))
  )


(defun get-property (object name package)
  (if (hash-table-p object)
      (gethash name object)
    (slot-value object (symb2 package name)))
  )

(defun count-back-chars(str chr pos)
  (if (and (> pos 0) (<= pos (length str))
	   (equal chr (char str (- pos 1))))
      (+ 1 (count-back-chars str chr(- pos 1)))
    0)
  )

(defun string-replace (str old new) 
  (let ((pos (search old str)))
    (if pos
	(format nil "~A~A~A"
		(subseq str 0 pos)
		new
		(string-replace (subseq str (+ pos (length old)))
				old new))
      str)    
    )
)


(defun update-struct (struct attrs values package)
  (if (and (car attrs) (car values))
      (progn
	(setf (slot-value struct
			  (symb2 package
				 (car attrs))
			  )
	      (car values))
	(update-struct struct (cdr attrs)
		       (cdr values) package)
	)
    struct
    )
  )

(defun contents-of-file (pathname)
    (with-open-file (in pathname :direction :input)
		(contents-of-stream in)))

(defun contents-of-stream (in)
  "Returns a string with the entire contents of the specified file."
  ;; This is excl:file-contents in ACL.
  (with-output-to-string (contents)
      (let* ((buffer-size 128)
             (buffer (make-string buffer-size)))
        (labels ((read-chunks ()
                   (let ((size (read-stream buffer in)))
                     (if (< size buffer-size)
                       (princ (subseq buffer 0 size) contents)
                       (progn
                         (princ buffer contents)
                         (read-chunks))))))
          (read-chunks)))
		contents
		))

(defun split-str (str delims)
  "Podzielic string na kawalki wedlug delimiterow"
  (if (and (car delims) (not (equal "" str)))
  	(let* ((pos (search (car delims) str))
			 (token (if pos (subseq str 0 pos))))
	  (nconc (list token)
				(split-str (if token (subseq str (+ (length (car delims)) 
																(length token)))
								 			str)
									 	(cdr delims))))
	(list str))
 )

(defun read-chars (str cnt)
  "Wczytac cnt znakow ze strumienia"
  (let ((buf (make-string cnt)))
		(read-stream buf str)
		buf)
  )

(defun read-stream (buf str &optional (pos 0))
  (if (< pos (length buf))
 	(let ((char (read-char str nil nil)))
	  (if char
		 (progn 
			(setf (aref buf pos) char)
			(read-stream buf str (+ pos 1)))
		 pos))
	pos)
  )

(defun make-substrs (begins ends str &optional (cnt 0))
  (if (< cnt (array-dimension begins 0))
	 (nconc (list (subseq str (aref begins cnt) (aref ends cnt)))
			  (make-substrs begins ends str (1+ cnt))))
  )

(defun update-struct-from-xml (xmlnode struct package)
  (dolist (subnode (cddr xmlnode))
	 (if (listp subnode)
	 	(setf (slot-value struct
								(symb2 package
										 (car subnode)))
				(third subnode)))
  )
  struct)

(defun nth-to-int (x element-number &key (junk-allowed T))
  "Zamienic n-ty element podlisty z listy na liczbe calkowita"
  (format t "~A~%" (nth element-number x))
  (if (nth element-number x)
	 (let* ((el (substitute 
					  (character "k") #\. 
					  (substitute (character "k") #\, (nth element-number x))))
			  (finel (remove-if-not #'digit-char-p  (subseq el 0
																			(position-if #'alpha-char-p el)))))
		(setf (nth element-number x)
				(parse-integer finel
								 :junk-allowed T))))
  x
  )

(defun flatten-list-of-lists (lst)
  (if (car lst)
	 (nconc (car lst)
			  (flatten-list-of-lists (cdr lst)))))

(defun strip-tags (string)
  (let* ((pos (position #\< string))
			(posend (if pos (position #\> string :start pos))))
	(if (and pos posend)
	  (format nil "~A~A" (subseq string 0 pos)
				 				(strip-tags (subseq string (1+ posend))))
	  string
	  )
  ))

(defun map-nth (list idx fun)
  (setf (nth idx list)
		  (funcall fun (nth idx list)))
  )

;
; $Log$
;
